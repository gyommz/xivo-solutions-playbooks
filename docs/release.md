# Release

This playbooks is for automating the differents releases process (start of iteration / bugfix / IV )

For the moment for the [Start of Iteration Checklist](https://gitlab.com/avencall/randd-doc/-/wikis/iteration/templates/First-IV-Iteration-Checklist) we have :
- Git repo
- Upload install scripts on the mirror

- Create a `release_vars.yml` file and fill it with the default variables below :

```shell
---
# Git repo
ssh_key_file: "~/.ssh/id_rsa" # Path to you private key for gitlab, this prevent the ask for username / password for each project
NEW_LTS_BRANCH: "2023.10" # The previous LTS VERSION, Luna if Maia is the next LTS
project_list: # Here list all the project from the set all project version jenkins builds
- name: "xucmgt"

# Upload install script
ansible_become_pass: "" # The password for the user root of the mirror
```

You can launch the playbook with :

```bash
ansible-playbook -i inventories/common/release release.yml
```

You can also launch specific role using tags by adding [refer to doc](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_tags.html)
```bash
--tags some_tag
```

For the upload install script on the mirror you will need to have a ssh config to the mirror to be able to use the role.
