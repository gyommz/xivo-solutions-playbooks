# XiVO Solutions Playbooks

This repository contains Ansible playbooks to manage XiVO applications

## Prerequisites

- Install Ansible. (version >= ansible-playbook [core 2.14.2])

## Playbooks Available

- [Upgrade](docs/upgrade.md): Manages the updating of XiVO applications
- [Release](docs/release.md): Manages the LTS releases

## How to use Playbooks

Please consult the specific README files for each playbook for detailed instructions on use.

## Editor config

For [Editor Config](https://editorconfig.org) to work on vscode please install the extension "EditorConfig for VS Code"
